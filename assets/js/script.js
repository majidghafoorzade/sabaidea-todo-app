//define global variables
const apiUrl = "http://api.aparat.com/fa/v1/video/video/mostViewedVideos";
const todoForm = document.getElementById("todo-form");
const todoInput = document.getElementById("todo-input");
const todoList = document.getElementById("todo-list");
const todoItems = [];
var editingItem = null;
var itemsCount = 0;



//add event listeners
todoForm.addEventListener('submit', submitForm);
todoList.addEventListener('click', handleActions);
document.body.addEventListener('click', handleCloseModal);
document.body.addEventListener('submit', handleEdit);
window.addEventListener('load', showSavedData);



//define Todo class
class Todo{

    //method to add item to list
    add(item){
        todoItems.push(item);
        this.render();
        itemsCount++;
    }



    //method to remove item from the list
    remove(index){
        todoItems.splice(index, 1);
        this.render();
    }



    //method to update item by index
    update(index, newValue){
        todoItems[index] = newValue;
        this.render();
    }


    //function to render items list
    render(){
        todoList.innerHTML = "";
        todoItems.forEach((item, index) => {
            createItem(item, index);
        });
        saveItems();
    }

}




const todo = new Todo();


//function to handle submit form
function submitForm(e){
    e.preventDefault();
    todo.add(todoInput.value);
    clearInput();
    if(itemsCount%5 === 0){
        getMostViewed();
    }
}






//function to create item element
function createItem(value, index){
    if(!value) {return}
    var item = handleCraete('div', 'item', null, todoList);
    item.itemId = index;
    handleCraete('div', 'item-text', value, item);
    var itemActions = handleCraete('div', 'actions', null, item);
    handleCraete('button', 'remove-item', "Remove", itemActions);
    handleCraete('button', 'edit-item', "Edit", itemActions);
}




//function to handle create and append elements
function handleCraete(el, className=null, text=null, appendIn=null){
    var element = document.createElement(el);
    if(className) {
        element.classList.add(className);
    }
    if(text) {
        element.textContent = text;
    }
    if(appendIn) {
        appendIn.appendChild(element);
    }
    return element;
}



//function to clear input after submit
function clearInput(){
    todoInput.value = "";
}



//function to handle items actions (Remove & Edit)
function handleActions(e){
    var thisItem = e.target.parentNode.parentNode;
    if(e.target.classList.contains('remove-item')){
        todo.remove(thisItem.itemId);
    } else if(e.target.classList.contains('edit-item')){
        let editForm = handleCraete('form', 'edit-form', null, null);
        let editInput = handleCraete('input', 'edit-input', null, editForm);
        editInput.type = "text";
        editInput.value = todoItems[thisItem.itemId];
        editingItem = thisItem.itemId;
        openModal(editForm);
    }
}


//function to handle display modal
function openModal(modalContent){
    var modal = handleCraete('div', 'modal', null, document.body);
    var modalBox = handleCraete('div', 'modal-box', null, modal);
    handleCraete('button', 'modal-close', 'close', modalBox);
    modalBox.appendChild(modalContent);
}


//close modal
function closeModal(){
    document.querySelector('.modal').remove();
}


//handle submit edit form
function handleEdit(e){
    e.preventDefault();
    if(e.target.classList.contains("edit-form")){
        let editInput = document.querySelector(".edit-input");
        todo.update(editingItem, editInput.value);
        closeModal();
    }
}


//function to get most viewed from aparat
function getMostViewed(){
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let response = JSON.parse(xhttp.responseText);
            let videoSrc = response.data[0].attributes.preview_src;
            createVideoModal(videoSrc);
        }
    };
    xhttp.open("GET", apiUrl, true);
    xhttp.send();
}


//function to create video modal
function createVideoModal(videoSrc){
    var video = handleCraete('video', null, null, null);
    video.src = videoSrc;
    video.controls = true;
    openModal(video);
}


//function to handle close modal
function handleCloseModal(e){
    if(e.target.classList.contains("modal-close")){
        closeModal();
    }
}


//function to save items in cookies
function saveItems(){
    var itemsStringify = JSON.stringify(todoItems);
    set_cookie('todoItems', itemsStringify);
}



//function to set cookie
function set_cookie(cookiename, cookievalue) {
    var date = new Date();
    date.setTime(date.getTime() + 24 * 3600 * 1000);
    document.cookie = cookiename + "=" + cookievalue + "; path=/;expires = " + date.toGMTString();

}


//get cookies by name
function getCookies(cookieName){
    var name = cookieName + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return false;
}



//show todoItems based on saved cookies
function showSavedData(){
    let todos = JSON.parse(getCookies('todoItems'));
    todos.forEach((item) => {
        todo.add(item);
    });
}